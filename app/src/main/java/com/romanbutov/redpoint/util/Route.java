package com.romanbutov.redpoint.util;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.romanbutov.redpoint.provider.RedpointContract;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.romanbutov.redpoint.provider.RedpointContract.Routes.*;

public class Route implements Parcelable {
    private static final String TAG = "Route";

    private String name;
    private Grade grade;
    private int sector;
    private int locationX;
    private int locationY;
    private String avatar;
    private Date createDate;
    private Date modifyDate;
    private int status;
    private String note;
    private static String[] sectors;

    public Route(Cursor c) {
        name = c.getString(c.getColumnIndex(ROUTE_NAME));
        grade = new Grade(c.getInt(c.getColumnIndex(ROUTE_GRADE)));
        sector = c.getInt(c.getColumnIndex(ROUTE_SECTOR));
        locationX = c.getInt(c.getColumnIndex(ROUTE_LOCATION_X));
        locationY = c.getInt(c.getColumnIndex(ROUTE_LOCATION_Y));
        avatar = c.getString(c.getColumnIndex(ROUTE_AVATAR_URL));
        status = c.getInt(c.getColumnIndex(ROUTE_STATUS));
        note = c.getString(c.getColumnIndex(ROUTE_NOTE));
        long cd = c.getLong(c.getColumnIndex(ROUTE_CREATE_DATE));
        createDate = new Date(cd);
        long md = c.getLong(c.getColumnIndex(ROUTE_MODIFY_DATE));
        modifyDate = new Date(md);
    }

    public static void initSectorNames(String[] sectorNames) {
        sectors = sectorNames.clone();
    }

    public String getName() {
        return name;
    }

    public String getGradeStr() {
        return grade.toString();
    }

    public int getSector() {
        return sector;
    }

    public String getLocationStr() {
        if (sectors == null)
            return "";
        String wall = "";
        int index = sector-1;
        if (index >= 0 && index < sectors.length)
            wall = sectors[index];
        return wall;
    }

    public float getXFactor() {
        return locationX / 100f;
    }

    public float getYFactor() {
        return locationY / 100f;
    }

    public String getAvatarRelativeUrl() {
        return avatar;
    }

    public String getAvatarUrl() {
        return avatar;
    }

    public String getDateDifference() {
        Date now = new Date();
        long millies = now.getTime() - createDate.getTime();
        long days = TimeUnit.DAYS.convert(millies, TimeUnit.MILLISECONDS);
        if (days < 1)
            return "сегодня";
        else if (days < 2)
            return "вчера";
        else if (days < 7)
            return String.format("%d дн", days);
        else if (days < 30)
            return String.format("%d нед", days / 7);
        else if (days < 12 * 30)
            return String.format("%d мес", days / 30);
        else if (days < 365 * 2)
            return "1 год";
        else if (days < 365 * 5)
            return String.format("%d года", days / 365);
        else
            return String.format("%d лет", days / 365);
    }

    public String getCreationDateStr() {
        SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy");
        return fmt.format(createDate);
    }

    public int getStatus() {
        return status;
    }

    public String getNote() {
        return note;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(grade.getNumeric());
        dest.writeInt(sector);
        dest.writeInt(locationX);
        dest.writeInt(locationY);
        dest.writeString(avatar);
        dest.writeLong(createDate.getTime());
        dest.writeLong(modifyDate.getTime());
        dest.writeInt(status);
        dest.writeString(note);
    }

    public static final Parcelable.Creator<Route> CREATOR
            = new Parcelable.Creator<Route>() {
        public Route createFromParcel(Parcel in) {
            return new Route(in);
        }

        public Route[] newArray(int size) {
            return new Route[size];
        }
    };

    private Route(Parcel in) {
        name = in.readString();
        grade = new Grade(in.readInt());
        sector = in.readInt();
        locationX = in.readInt();
        locationY = in.readInt();
        avatar = in.readString();
        createDate = new Date(in.readLong());
        modifyDate = new Date(in.readLong());
        status = in.readInt();
        note = in.readString();
    }
}