package com.romanbutov.redpoint.util;

import android.support.annotation.NonNull;

import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Created by ei7 on 21.01.17.
 */

public class Grade implements Comparable<Grade> {

        /* from java tutorial:
    Name objects are immutable. All other things being equal, immutable types are the way to go,
    especially for objects that will be used as elements in Sets or as keys in Maps.
    These collections will break if you modify their elements or keys while they're in the collection.
    The constructor checks its arguments for null. This ensures that all Name objects are well
    formed so that none of the other methods will ever throw a NullPointerException.
    The hashCode method is redefined. This is essential for any class that redefines the equals
    method. (Equal objects must have equal hash codes.)
    The equals method returns false if the specified object is null or of an inappropriate type.
    The compareTo method throws a runtime exception under these circumstances.
    Both of these behaviors are required by the general contracts of the respective methods.
    The toString method has been redefined so it prints the Name in human-readable form.
    This is always a good idea, especially for objects that are going to get put into collections.
    The various collection types' toString methods depend on the toString methods of their
    elements, keys, and values.
         */

    // digit is for example 7 in grade "7a+"
    private final int digit;
    // letter is for example A in grade "7a+"
    private final Letter letter;
    // sign is for example PLUS in "7a+", NONE in "7a", MINUS in "7a-"
    private final Sign sign;

    public static final Grade minGrade = new Grade(5, Letter.A, Sign.NONE);
    public static final Grade maxGrade = new Grade(8, Letter.A, Sign.NONE);
    private static final SortedSet<Grade> allUnsignedGrades = new TreeSet<>();

    static {
        Grade g = minGrade.getUnsigned();
        while (g.compareTo(maxGrade)<=0)
        {
            allUnsignedGrades.add(g);
            g = g.next();
        }
    }

    // TODO: assert value in range 50..89
    // TODO: what approach to grade names without letter, like "5"?

    public Grade(int digit, Letter letter, Sign sign) {
        this.digit = digit;
        this.letter = letter;
        this.sign = sign;
    }

    public Grade(int digit, Letter letter) {
        this(digit, letter, Sign.NONE);
    }

    public Grade(Grade g) {
        this.digit = g.digit;
        this.letter = g.letter;
        this.sign = g.sign;
    }

    public Grade(int numeric) {
        this.digit = numeric / 10;
        int letterAndSign = numeric % 10 - 1;
        // to avoid unnecessary complexity, we allow numeric representation be for example 50 or 60
        // we consider it as 51 and 61, i.e. "5a-" and "6a-" grade
        if (letterAndSign < 0)
            letterAndSign = 0;
        letter = fetchLetter(letterAndSign);
        sign = getSign(letterAndSign);
    }

    public static SortedSet<Grade> getAllUnsignedGrades() {
        return allUnsignedGrades;
    }

    public int getNumeric() {
        int base = digit * 10 + 1;
        int l = letter.ordinal() * 3;
        int s = sign.ordinal();
        return  (base + l + s);
    }

    public Grade next() {
        // TODO: exception if Grade > 9b
        int d = digit;
        if (letter==Letter.C)
            d++;
        return new Grade(d, letter.next(), sign.NONE);
    }

    @Override
    public String toString() {
        return digit + letter.toString() + sign.toString();
    }

    public Grade getUnsigned() {
        return new Grade(this.digit, this.letter, Sign.NONE);
    }

    public Grade getPlus() {
        return new Grade(this.digit, this.letter, Sign.PLUS);
    }

    public Grade getMinus() {
        return new Grade(this.digit, this.letter, Sign.MINUS);
    }

    @NonNull
    private Sign getSign(int letterAndSign) {
        int signNum = letterAndSign % 3;
        return Sign.values()[signNum];
    }

    @NonNull
    private Letter fetchLetter(int letterAndSign) {
        int letterNum = letterAndSign / 3;
        return Letter.values()[letterNum];
    }

    @Override
    public int compareTo(Grade another) {
        return getNumeric() - another.getNumeric();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Grade grade = (Grade) o;

        if (digit != grade.digit) return false;
        if (letter != grade.letter) return false;
        return sign == grade.sign;

    }

    @Override
    public int hashCode() {
        int result = digit;
        result = 31 * result + letter.hashCode();
        result = 31 * result + sign.hashCode();
        return result;
    }

    public enum Letter {
        A, B, C;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }

        public Letter next() {
            int ord = ordinal()+1;
            if(ord >= values().length)
                ord = 0;
            return values()[ord];
        }
    }

    public enum Sign {
        MINUS, NONE, PLUS;

        @Override
        public String toString() {
            switch (ordinal()) {
                case 0:
                    return "-";
                case 1:
                    return "";
                case 2:
                    return "+";
            }
            return "";
        }
    }
}