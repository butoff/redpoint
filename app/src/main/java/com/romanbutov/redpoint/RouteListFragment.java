package com.romanbutov.redpoint;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ResourceCursorAdapter;
import android.widget.TextView;

import com.romanbutov.redpoint.util.Grade;
import com.romanbutov.redpoint.util.Route;
import com.romanbutov.redpoint.provider.RedpointContract;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;

/**
 * A fragment representing list of routes.
 */
public class RouteListFragment
        extends Fragment
        implements LoaderManager.LoaderCallbacks<Cursor>,
        RouteListAdapter.ClickListener {
    private static final String TAG = "RouteListFragment";

    private static final String PREFERENCES_FIRST_LAUNCH = "firstLaunch";
    private static final Grade MIN_GRADE = new Grade(5, Grade.Letter.A, Grade.Sign.NONE);
    private static final Grade MAX_GRADE = new Grade(8, Grade.Letter.A, Grade.Sign.NONE);

    @BindView(R.id.route_list)
    RecyclerView recyclerView;
    @BindView(R.id.progress_routes)
    ProgressBar progressBar;
    private Unbinder unbinder;

    private Cursor cursor;
    private RouteListActivity activity;
    private boolean isTwoPane;
    private RouteListAdapter adapter;
    private Grade gradeFrom;
    private Grade gradeTo;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (RouteListActivity) getActivity();
        initAdapter();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume: start");
        super.onResume();
        loadGrades();
        requery();
    }

    private void loadGrades() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        int from = preferences.getInt("pref_fromGrade", MIN_GRADE.getNumeric());
        int to = preferences.getInt("pref_toGrade", MAX_GRADE.getNumeric());
        //TODO: "grade plus-minus" logic (example: 6c -> 6c+)
        gradeFrom = new Grade(from);
        gradeTo = new Grade(to);
    }

    private void requery() {
        showProgress();
        getLoaderManager().restartLoader(0, null, this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.route_list_frag, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "onDestroyView: undinder.undind()");
        unbinder.unbind();
    }

    private void startDetail(Route route) {
        Intent intent = new Intent(activity, RouteDetailActivity.class);
        intent.putExtra(Route.class.getCanonicalName(), route);
        startActivity(intent);
    }

    private void initAdapter() {
        adapter = new RouteListAdapter(activity, null, this);
        recyclerView.setAdapter(adapter);
    }

    public void showProgress() {
        if (progressBar != null)
            progressBar.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        if (progressBar != null)
            progressBar.setVisibility(View.GONE);
    }

    public void setTwoPane(boolean isTwoPane) {
        this.isTwoPane = isTwoPane;
    }

    private boolean isFirstLaunch() {
        SharedPreferences sp = getActivity().getPreferences(Context.MODE_PRIVATE);
        boolean fl = sp.getBoolean(PREFERENCES_FIRST_LAUNCH, true);
        if (fl)
            sp.edit().putBoolean(PREFERENCES_FIRST_LAUNCH, false).commit();
        return fl;
    }

    private void showDetails() {
        // since we drop details activity in version 1.0, do nothing
/*
                    if (isTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(RouteDetailFragment.ARG_ITEM_ID, holder.mItem.pk);
                        RouteDetailFragment fragment = new RouteDetailFragment();
                        fragment.setArguments(arguments);
                        ((AppCompatActivity)getActivity()).getSupportFragmentManager().beginTransaction()
                                .replace(R.id.route_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = getActivity();
                        Intent intent = new Intent(context, RouteDetailActivity.class);
                        intent.putExtra(RouteDetailFragment.ARG_ITEM_ID, holder.mItem.pk);
                        context.startActivity(intent);
                    }
*/
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(getClass().getSimpleName(), "onCreateLoader()");
        String fmtStr = "((%s>=%d) AND (%s<=%d) AND (%s=0))";
        String selection = String.format(Locale.US, fmtStr,
                RedpointContract.Routes.ROUTE_GRADE, gradeFrom.getNumeric(),
                RedpointContract.Routes.ROUTE_GRADE, gradeTo.getNumeric(),
                RedpointContract.Routes.ROUTE_STATUS);
        Log.d(getClass().getSimpleName(), "selection=" + selection);
        return new CursorLoader(getActivity(), RedpointContract.BASE_CONTENT_URI, null, selection, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor c) {
        Log.d(getClass().getSimpleName(), "onLoadFinished(), routes loaded:" + c.getCount());
        hideProgress();
        updateListView(c);
        updateActionBar(c.getCount());
    }

    private void updateActionBar(int count) {
        CharSequence title = getActivity().getTitle();
        if (gradeFrom.compareTo(MIN_GRADE) >= 0 || gradeTo.compareTo(MAX_GRADE) <= 0) {
            Grade from = gradeFrom.getUnsigned();
            Grade to = gradeTo.getUnsigned();
            String fmtStr = getString(R.string.routes_range_title);
            title = String.format(fmtStr, from, to, count);
        }
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(title);
    }

    private void updateListView(Cursor c) {
        cursor = c;
        adapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        hideProgress();
        updateListView(null);
    }

    @Override
    public void onClick(int position) {
        if (cursor != null) {
            cursor.moveToPosition(position);
            Route route = new Route(cursor);
            startDetail(route);
        }
    }
}
