package com.romanbutov.redpoint;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.romanbutov.redpoint.util.Route;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @author Roman Butov
 */

class RouteListAdapter extends RecyclerView.Adapter<RouteListAdapter.RouteViewHolder> {

    private static final String TAG = "RouteListAdapter";

    private Context context;
    private Cursor cursor;
    private ClickListener listener;

    RouteListAdapter(Context context, Cursor cursor, ClickListener listener) {
        this.context = context;
        this.cursor = cursor;
        this.listener = listener;
    }

    @Override
    public RouteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.route_list_content, parent, false);
        return new RouteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RouteViewHolder holder, final int position) {
        if (cursor == null)
            return;
        cursor.moveToPosition(position);
        Route route = new Route(cursor);
        holder.name.setText(route.getName());
        holder.grade.setText(route.getGradeStr());
        holder.location.setText(route.getLocationStr());
        holder.date.setText(route.getDateDifference());
        String avaUrl = route.getAvatarUrl();
        bindAvatar(context, holder.avatar, avaUrl, R.drawable.ic_ava1, R.drawable.ic_ava1);
        holder.position = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(position);
            }
        });
    }

    private void bindAvatar(Context context, ImageView view, String avaUrl, int placeholder, int error) {
        Picasso.with(context).load(avaUrl)
                .transform(new Transformation() {
                    @Override
                    public Bitmap transform(Bitmap source) {
                        int size = Math.min(source.getWidth(), source.getHeight());

                        int x = (source.getWidth() - size) / 2;
                        int y = (source.getHeight() - size) / 2;

                        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
                        if (squaredBitmap != source) {
                            source.recycle();
                        }

                        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

                        Canvas canvas = new Canvas(bitmap);
                        Paint paint = new Paint();
                        BitmapShader shader = new BitmapShader(squaredBitmap,
                                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
                        paint.setShader(shader);
                        paint.setAntiAlias(true);

                        float r = size / 2f;
                        canvas.drawCircle(r, r, r, paint);

                        squaredBitmap.recycle();
                        return bitmap;
                    }

                    @Override
                    public String key() {
                        return "circle";
                    }
                })
                //.fit().centerCrop()
                .placeholder(placeholder)
                .error(error)
                .into(view);
    }

    @Override
    public int getItemCount() {
        return (cursor == null) ? 0 : cursor.getCount();
    }

    /**
     * Returns the Cursor pointed to the route corresponding to the position in the list
     * @param position
     * @return
     */
//    public Cursor getItem(int position) {
//        if (cursor != null) {
//            long id =
//            cursor.moveToPosition(id);
//        }
//        return cursor;
//    }

    /**
     * Swaps the Cursor currently held in the adapter with a new one
     * and triggers a UI refresh
     *
     * @param newCursor the new cursor that will replace the existing one
     */
    public void swapCursor(Cursor newCursor) {
        Log.d(TAG, "swapCursor() called with: newCursor = [" + newCursor + "]");

        // Always close the previous mCursor first
        if (cursor != null) cursor.close();
        cursor = newCursor;
        if (newCursor != null) {
            // Force the RecyclerView to refresh
            this.notifyDataSetChanged();
        }
        Log.d(TAG, "swapCursor() returned");
    }

    class RouteViewHolder extends RecyclerView.ViewHolder {
        // TODO: 15.05.17 getItemId(), stable ID's and so on - read SDK carefully

        RouteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        int position;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.grade)
        TextView grade;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.avatar)
        ImageView avatar;
    }

    interface ClickListener {
        void onClick(int position);
    }
}
