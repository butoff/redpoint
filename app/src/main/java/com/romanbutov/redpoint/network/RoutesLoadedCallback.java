package com.romanbutov.redpoint.network;

public interface RoutesLoadedCallback {
    void onRoutesLoaded();
}
