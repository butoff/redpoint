package com.romanbutov.redpoint.network;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ServerInterface {

    // TODO: change {resource} to static string "routes", and drop (String resource) argument
    @GET("{resource}")
    Call<List<RouteTransport>> routes(@Path("resource") String resource,
                                      @Query("format") String format, @Query("since") String date);

    @POST("routes")
    Call<RouteTransport> addRoute(@Body RouteTransport route);
}
