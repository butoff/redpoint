package com.romanbutov.redpoint.network;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for REST interaction with server
 */
public class NetworkHelper implements Callback<List<RouteTransport>> {

    private static final String TAG = "NetworkHelper";

    public static final String SERVER_URL = "http://butoff.pythonanywhere.com";
    public static final String MEDIA_PATH = "media";
    public static final String ROUTES_PATH = "routes";
    public static final String MEDIA_URL = SERVER_URL + "/" + MEDIA_PATH + "/";

    private List<RouteTransport> routes;
    private RoutesLoadedCallback loadedCallback;
    private Retrofit retrofit;

    public boolean isSuccess;
    public int status;
    public String message;

    public NetworkHelper(RoutesLoadedCallback loadedCallback, Gson gson) {
        this.loadedCallback = loadedCallback;
        retrofit = new Retrofit.Builder()
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    public List<RouteTransport> getRoutes() {
        return routes;
    }

    public void load(Date lastUpdate) {
        ServerInterface serverInterface = retrofit.create(ServerInterface.class);
        SimpleDateFormat fmt = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss Z");
        String lastUpdateStr = fmt.format(lastUpdate);
        Call<List<RouteTransport>> call = serverInterface.routes(ROUTES_PATH, "json", lastUpdateStr);
        call.enqueue(this);
    }

    public void addRoute(RouteTransport route) {
        // TODO: extract to member variable ?
        ServerInterface serverInterfaces = retrofit.create(ServerInterface.class);
        Call<RouteTransport> call = serverInterfaces.addRoute(route);
        call.enqueue(new Callback<RouteTransport>() {
            @Override
            public void onResponse(Call<RouteTransport> call, Response<RouteTransport> response) {

                isSuccess = response.isSuccess();
                status = response.code();
                message = response.message();

                loadedCallback.onRoutesLoaded();
            }

            @Override
            public void onFailure(Call<RouteTransport> call, Throwable t) {
                loadedCallback.onRoutesLoaded();
            }
        });
    }

    public boolean addRouteSynchronously(RouteTransport route) {
        // TODO: extract to member variable ?
        ServerInterface serverInterfaces = retrofit.create(ServerInterface.class);
        Call<RouteTransport> call = serverInterfaces.addRoute(route);
        try {
            call.execute();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    @Override
    public void onResponse(Call<List<RouteTransport>> call, Response<List<RouteTransport>> response) {
        Log.d(TAG, "onResponse: start");

        if(response.isSuccess()) {
            Log.d(TAG, "onResponse: success!");
            routes = response.body();
        }
        else {
            Log.d(TAG, "onResponse: failure...");
            // TODO: some error handling?
        }
        if(loadedCallback != null)
            loadedCallback.onRoutesLoaded();
    }

    @Override
    public void onFailure(Call<List<RouteTransport>> call, Throwable t) {
        Log.d(TAG, "onFailure() called with: call = [" + call + "], t = [" + t + "]");
    }
}
