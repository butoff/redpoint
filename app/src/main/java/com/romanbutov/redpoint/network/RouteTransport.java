package com.romanbutov.redpoint.network;

import java.util.Date;

/**
 * Created by ei7 on 26.01.17.
 */

public final class RouteTransport {
    public String id;
    public String name;
    public int grade;
    public int status;
    public Date create_date;
    public Date modify_date;
    public String note;
    public String avatar;
    public int sector;
    public int location_x;
    public int location_y;

    public RouteTransport() {
        create_date = new Date();
        modify_date = create_date;
    }

    @Override
    public String toString() {
        return name;
    }
}
