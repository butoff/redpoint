package com.romanbutov.redpoint;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import com.romanbutov.redpoint.util.Grade;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by ei7 on 04.02.17.
 */

public class GradePickerPreference extends DialogPreference {
    // TODO: keyboard sould not be shown while interaction with grade picker
    // (that bug appears at Oysters T72HM3G  and don't at ZTE)

    private static final String TAG = GradePickerPreference.class.getSimpleName();

    private static final Grade MIN_GRADE = new Grade(5, Grade.Letter.A);
    private static final Grade MAX_GRADE = new Grade(8, Grade.Letter.A);
    private static final int DEFAULT_VALUE = 51;

    private List<Grade> grades;
    private int value = DEFAULT_VALUE;
    private NumberPicker picker;
    // true, if instance is "from" grade preference, and false for "to"
    private boolean isFrom;
    // assuming that there is only two instances of this class,
    // we use two static variables in order to properly initialize picker values
    private static Grade fromGrade;
    private static Grade toGrade;

    public GradePickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "GradePickerPreference()");
//        printAttrs(attrs);

        setFromOrTo(attrs);
        setDialogLayoutResource(R.layout.gradepicker_dialog);
    }

    private void printAttrs(AttributeSet attrs) {
        String attStr = "";
        for (int i = 0; i < attrs.getAttributeCount(); i++) {
            String attr = attrs.getAttributeName(i);
            String val = attrs.getAttributeValue(i);
            attStr = attStr + "\n" + attr + " = " + val;
        }
        Log.d(TAG, "printAttrs: " + attStr);
    }

    private void setFromOrTo(AttributeSet attrs) {
        String key = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "key");
        String errMsg = "android:key attribute must be set for GradePickerPreference";
        if (key == null)
            throw new RuntimeException(errMsg);
        else if (key.equals("pref_fromGrade"))
            isFrom = true;
        else if (key.equals("pref_toGrade"))
            isFrom = false;
        else
            throw new RuntimeException(errMsg);
    }

    @Override
    protected View onCreateDialogView() {
        Log.d(TAG, "onCreateDialogView()");

        View view = super.onCreateDialogView();
        picker = (NumberPicker) view.findViewById(R.id.grade_picker);
        initGrades();
        initPicker();
        return view;
    }

    private void initGrades() {
        SortedSet<Grade> gradesSet;
        if (isFrom) {
            gradesSet = Grade.getAllUnsignedGrades().subSet(MIN_GRADE, toGrade);
            grades = new ArrayList<>(gradesSet);
            grades.add(toGrade);
        } else {
            gradesSet = Grade.getAllUnsignedGrades().subSet(fromGrade, MAX_GRADE);
            grades = new ArrayList<>(gradesSet);
            grades.add(MAX_GRADE);
        }
    }

    private void initPicker() {
        int count = grades.size();
        String[] labels = new String[count];
        for (int i = 0; i < count; i++)
            labels[i] = grades.get(i).toString();
        picker.setMinValue(0);
        picker.setMaxValue(count - 1);
        picker.setDisplayedValues(labels);
        picker.setWrapSelectorWheel(false);
        setPickerValue();
    }

    private void setPickerValue() {
        Grade current = new Grade(value);
        int currentIndex = grades.indexOf(current);
        picker.setValue(currentIndex);

    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        Log.d(TAG, "onDialogClosed()");

        if (positiveResult) {
            Grade grade = grades.get(picker.getValue());
            value = grade.getNumeric();
            setCurrentGradeSecondaryText();
            persistInt(value);
            setFromOrToGrade();
        }
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        Log.d(TAG, "onSetInitialValue()");

        if (restorePersistedValue)
            value = this.getPersistedInt(DEFAULT_VALUE);
        else {
            value = (Integer) defaultValue;
            persistInt(value);
        }
        setCurrentGradeSecondaryText();
        setFromOrToGrade();
    }

    private void setFromOrToGrade() {
        if (isFrom)
            fromGrade = new Grade(value);
        else
            toGrade = new Grade(value);
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInteger(index, DEFAULT_VALUE);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Log.d(TAG, "onSaveInstanceState()");

        final Parcelable superState = super.onSaveInstanceState();
        // Check whether this Preference is persistent (continually saved)
        if (isPersistent()) {
            // No need to save instance state since it's persistent,
            // use superclass state
            return superState;
        }

        // Create instance of custom BaseSavedState
        final SavedState myState = new SavedState(superState);
        // Set the state's value with the class member that holds current
        // setting value
        value = grades.get(picker.getValue()).getNumeric();
        myState.value = value;
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState()");

        // Check whether we saved the state in onSaveInstanceState
        if (state == null || !state.getClass().equals(SavedState.class)) {
            // Didn't save the state, so call superclass
            super.onRestoreInstanceState(state);
            return;
        }

        // Cast state to custom BaseSavedState and pass to superclass
        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());

        // Set this Preference's widget to reflect the restored state
        value = myState.value;
        setPickerValue();
    }

    public void setCurrentGradeSecondaryText() {
        Grade g = new Grade(value);
        setSummary(g.toString());
    }

    private static class SavedState extends BaseSavedState {
        // Member that holds the setting's value
        // Change this data type to match the type saved by your Preference
        int value;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            // Get the current preference's value
            value = source.readInt();  // Change this to read the appropriate data type
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // Write the preference's value
            dest.writeInt(value);  // Change this to write the appropriate data type
        }

        // Standard creator object using an instance of this class
        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {

                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}
