package com.romanbutov.redpoint;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.romanbutov.redpoint.util.Route;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;


/**
 * Created by ei7 on 19.02.17.
 */

public class LocationView extends View {
    private static final String TAG = "LocationView";

    private Route route;
    private Drawable locationImage;
    private RoundedBitmapDrawable avatar;

    private int width;
    private int height;

    // TODO: 28.02.17 density independent pixels in radius measurement
    private static final int RADIUS = 35;
    private static final int MARGIN = 2;
    private static final int IMAGES[] = {R.drawable.s1, R.drawable.s2, R.drawable.s3,
            R.drawable.s4, R.drawable.s5, R.drawable.s6, R.drawable.s7, R.drawable.s8};

    public LocationView(Context context) {
        super(context);
    }

    public LocationView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LocationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.d(TAG, "onDraw() called with: canvas = [" + canvas + "]");
        if (locationImage == null) {
            super.onDraw(canvas);
            return;
        }
        locationImage.setBounds(0, 0, width, height);
        locationImage.draw(canvas);
        if (avatar != null)
            drawAvatar(canvas);
    }

    private void drawAvatar(Canvas canvas) {
        int centerVertical = calculateCenterVertical();
        int centerHorizontal = calculateCenterHorizontal();
        avatar.setBounds(centerHorizontal - RADIUS, centerVertical - RADIUS,
                centerHorizontal + RADIUS, centerVertical + RADIUS);
        avatar.draw(canvas);
    }

    private int calculateCenterVertical() {
        float factorVertical = route.getYFactor();
        int centerVertical = Math.round(height * factorVertical);
        if (centerVertical < RADIUS + MARGIN)
            centerVertical = RADIUS + MARGIN;
        if (centerVertical > height - (RADIUS + MARGIN))
            centerVertical = height - (RADIUS + MARGIN);
        return centerVertical;
    }

    private int calculateCenterHorizontal() {
        float factorHorizontal = route.getXFactor();
        int centerHorizontal = Math.round(width * factorHorizontal);
        if (centerHorizontal < RADIUS + MARGIN)
            centerHorizontal = RADIUS + MARGIN;
        if (centerHorizontal > width - (RADIUS + MARGIN))
            centerHorizontal = width - (RADIUS + MARGIN);
        return centerHorizontal;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.d(TAG, "onMeasure() called with: widthMeasureSpec = [" + widthMeasureSpec + "], heightMeasureSpec = [" + heightMeasureSpec + "]");
        if (route == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }
        setLocationImage();
        width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        float ih = locationImage.getIntrinsicHeight();
        float iw = locationImage.getIntrinsicWidth();
        float aspect = ih / iw;
        height = Math.round(width * aspect);
        setMeasuredDimension(width, height);
    }

    private void setLocationImage() {
        int imgRes = IMAGES[route.getSector() - 1];
        locationImage = ContextCompat.getDrawable(getContext(), imgRes);
    }

    public void setRoute(Route route) {
        this.route = route;
        loadAvatar();
    }

    private void loadAvatar() {
        if (route == null)
            return;
        String avaUrl = route.getAvatarUrl();
        Picasso.with(getContext())
                .load(avaUrl)
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        avatar = RoundedBitmapDrawableFactory.create(getContext().getResources(), bitmap);
                        avatar.setCircular(true);
                        invalidate();
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
    }
}
