package com.romanbutov.redpoint;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.romanbutov.redpoint.util.Route;
import com.romanbutov.redpoint.util.SimpleEasterEgg;

/**
 * An activity representing a list of Routes. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link RouteDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class RouteListActivity extends AppCompatActivity {

    private RouteListFragment routeListFragment;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private SimpleEasterEgg easter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_list);
        initModel();
        initToolbar();
        initFAB();
        fab.hide();
        initFragment();
    }

    private void initModel() {
        Resources res = getResources();
        String sectors[] = res.getStringArray(R.array.sectors);
        Route.initSectorNames(sectors);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_route_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                showSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showSettings() {
        startActivity(new Intent(this, RedpointPreferencesActivity.class));
    }

    private void initFragment() {
        FragmentManager fm = getFragmentManager();
        routeListFragment = (RouteListFragment) fm.findFragmentById(R.id.route_list_frag);

        if (findViewById(R.id.route_detail_container) != null)
            routeListFragment.setTwoPane(true);
    }

    private void initFAB() {
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doSomethingWithClickedFAB();
            }
        });
    }

    private void doSomethingWithClickedFAB() {
        startActivity(new Intent(this, NewRouteActivity.class));
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (easter.click())
//                toggleFAB();
            }
        });
    }

    public void toggleFAB() {
        if (fab.isShown())
            fab.hide();
        else
            fab.show();
    }
}
