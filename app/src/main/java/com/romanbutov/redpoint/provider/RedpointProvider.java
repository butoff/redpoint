package com.romanbutov.redpoint.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.romanbutov.redpoint.R;
import com.romanbutov.redpoint.network.NetworkHelper;
import com.romanbutov.redpoint.network.RouteTransport;
import com.romanbutov.redpoint.network.RoutesLoadedCallback;

import static com.romanbutov.redpoint.provider.RedpointContract.Routes.*;
import static com.romanbutov.redpoint.provider.RedpointContract.*;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * ContentProvider for routes database.
 * <p>
 * Data cached locally in SQL database, and RedpointProvider is in charge of synchronization
 * with server.
 * <p>
 * Data is synchronized periodically,
 * with time interval syncInterval (should be passed to RedpointProvider).
 */
public class RedpointProvider extends ContentProvider
        implements RoutesLoadedCallback, RedpointSQLiteHelper.CreatingListener {

    // TODO: shipping initial local database for better user experience during first launch
    // TODO: 28.02.17 move initial json to resource

    private static final String TAG = "RedpointProvider";

    private static final String PREFERENCES_LAST_UPDATE = "lastUpdate";
    private static final String SP_TAG = "RedpointProvider";
    // for local database debug purpose: true, if we won't load data from server
    // MUST be false normally
    private static final boolean WITHOUT_SINCHRONIZATION = false;

    private boolean isInitialized;
    private boolean isDBNewlyCreated;
    private boolean isSynchronized = true;

    private SharedPreferences sp;
    private RedpointSQLiteHelper db;
    private NetworkHelper network;
    private Gson gson;

    private Date dateBeforeUpdate;
    private Date lastUpdate;
    private final int syncInterval = 24;

    @Override
    public boolean onCreate() {
        Log.d(TAG, "onCreate() called");

        // since we can't perform heavy jobs in onCreate(), all initialization well be made in
        // initLazy() method after first query to RedpointProvider
        return true;
    }

    private void initLazy() {
        Log.d(TAG, "initLazy() called");

        if (isInitialized)
            return;

        sp = getContext().getSharedPreferences(SP_TAG, Context.MODE_PRIVATE);
        lastUpdate = new Date(sp.getLong(PREFERENCES_LAST_UPDATE, 0));
        db = new RedpointSQLiteHelper(getContext(), this);
        gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();
        network = new NetworkHelper(this, gson);
        isInitialized = true;
    }

    private void makeDBFromScratchIfNeeded() {
        if (isDBNewlyCreated) {
            makeDBFromScratch();
            isDBNewlyCreated = false;
        }
    }

    // TODO: 01.03.17 set here lastUpdate supplied with initial data
    // TODO: 05.03.17 IO error handling, stream closing
    private void makeDBFromScratch() {
        Log.d(TAG, "makeDBFromScratch() called");
        InputStream is = getContext().getResources().openRawResource(R.raw.initial);
        Reader reader = new BufferedReader(new InputStreamReader(is));
        RouteTransport[] routesArr = gson.fromJson(reader, RouteTransport[].class);
        fillDBWithRoutes(Arrays.asList(routesArr));
    }

    private void startSynchronize() {
        Log.d(TAG, "startSynchronize() called");

        // To avoid time gap, we store current date before query to server.
        // Later, in callback, we check the result, and if query is success,
        // assign dateBeforeUpdate to lastUpdate and persist it in preferences
        dateBeforeUpdate = new Date();
        network.load(lastUpdate);
    }

    private void persistLastUpdate() {
        Log.d(TAG, "persistLastUpdate() called");

        lastUpdate = dateBeforeUpdate;
        sp.edit().putLong(PREFERENCES_LAST_UPDATE, dateBeforeUpdate.getTime()).apply();
    }

    private boolean isSynchronizationNeeded() {
        Date now = new Date();
        long millis = now.getTime() - lastUpdate.getTime();
//        long hours = TimeUnit.HOURS.convert(millis, TimeUnit.MILLISECONDS);
        long hours = TimeUnit.SECONDS.convert(millis, TimeUnit.MILLISECONDS);
        if (hours > syncInterval) {
            Log.d(TAG, "isSynchronizationNeeded: lastUpdate: " + lastUpdate + ", now: " + now + ", hours: " + hours);
            isSynchronized = false;
        }
        return !isSynchronized;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Log.d(TAG, "query() called with: uri = [" + uri + "], projection = [" + projection + "]," +
                "selection = [" + selection + "], selectionArgs = [" + selectionArgs + "], sortOrder = [" + sortOrder + "]");

        initLazy();
        if (isSynchronizationNeeded() && !WITHOUT_SINCHRONIZATION)
            startSynchronize();

        final SQLiteDatabase rdb = db.getReadableDatabase();
        makeDBFromScratchIfNeeded();

        String columns[] = null;
        String groupBy = null;
        String having = null;
        String orderBy = null;
        Cursor cursor = rdb.query(TABLE_NAME, columns, selection, selectionArgs, groupBy, having, orderBy);
        ContentResolver cr = getContext().getContentResolver();
        if (cr != null && cursor != null)
            cursor.setNotificationUri(cr, BASE_CONTENT_URI);

        Log.d(TAG, "query() returned: " + cursor);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        initLazy();
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        initLazy();
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        initLazy();
        return 0;
    }

    @Override
    public void onRoutesLoaded() {
        Log.d(TAG, "onRoutesLoaded() called");

        // since update is a success, we should store last update time
        persistLastUpdate();
        List<RouteTransport> routes = network.getRoutes();
        fillDBWithRoutes(routes);
        isSynchronized = true;
        ContentResolver cr = getContext().getContentResolver();
        if (cr != null)
            cr.notifyChange(BASE_CONTENT_URI, null, false);
    }

    private void fillDBWithRoutes(List<RouteTransport> routes) {
        ContentValues cv = new ContentValues();
        for (RouteTransport r : routes) {
            String noteStr = r.note == null ? "null" : r.note;
            Log.d(TAG, "fillDBWithRoutes: " + r.name + "; status = " + r.status + "; note = " + noteStr);

            cv.put(ROUTE_PK, r.id);
            cv.put(ROUTE_NAME, r.name);
            cv.put(ROUTE_SECTOR, r.sector);
            cv.put(ROUTE_LOCATION_X, r.location_x);
            cv.put(ROUTE_LOCATION_Y, r.location_y);
            cv.put(ROUTE_GRADE, r.grade);
            cv.put(ROUTE_CREATE_DATE, r.create_date.getTime());
            cv.put(ROUTE_MODIFY_DATE, r.modify_date.getTime());
            cv.put(ROUTE_AVATAR_URL, r.avatar);
            cv.put(ROUTE_STATUS, r.status);
            cv.put(ROUTE_NOTE, r.note);
            db.getWritableDatabase().insert(TABLE_NAME, null, cv);
        }
        Log.d(TAG, "fillDBWithRoutes: bd filled successfully, routes inserted: " + routes.size());
    }

    /**
     * Invoked by onCreate() method of RedpointSQLiteHelper if database was (accidentally) deleted.
     * In that case, we should restore local database from server.
     */
    @Override
    public void onDatabaseNewlyCreated() {
        Log.d(TAG, "onDatabaseNewlyCreated() called");
        // we can't do anything with db here!
        isDBNewlyCreated = true;
    }
}