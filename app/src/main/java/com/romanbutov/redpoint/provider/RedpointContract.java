package com.romanbutov.redpoint.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract class for interacting with RedpointProvider
 */
public class RedpointContract {

    public static final String CONTENT_TYPE_APP_BASE = "redpoint.";

    public static final String CONTENT_TYPE_BASE = "vnd.android.cursor.dir/vnd."
            + CONTENT_TYPE_APP_BASE;

    public static final String CONTENT_ITEM_TYPE_BASE = "vnd.android.cursor.item/vnd."
            + CONTENT_TYPE_APP_BASE;

    public static final String CONTENT_AUTHORITY = "com.romanbutov.redpoint";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);


    public static abstract class Routes implements BaseColumns {
        private static final String ENTRY_NAME = "route";
        private static final String COLUMN_BASE = ENTRY_NAME + "_";

        public static final String TABLE_NAME = ENTRY_NAME + "s";

        public static final String ROUTE_PK = COLUMN_BASE + "pk";
        public static final String ROUTE_NAME = COLUMN_BASE + "name";
        public static final String ROUTE_GRADE = COLUMN_BASE + "grade";
        public static final String ROUTE_LOCATION_X = COLUMN_BASE + "location_x";
        public static final String ROUTE_LOCATION_Y = COLUMN_BASE + "location_y";
        public static final String ROUTE_AVATAR_URL = COLUMN_BASE + "avatar_url";
        public static final String ROUTE_CREATE_DATE = COLUMN_BASE + "create_date";
        public static final String ROUTE_MODIFY_DATE = COLUMN_BASE + "modify_date";
        public static final String ROUTE_STATUS = COLUMN_BASE + "status";
        public static final String ROUTE_NOTE = COLUMN_BASE + "note";
        public static final String ROUTE_SECTOR = COLUMN_BASE + "sector";
    }

    private RedpointContract() {}
}

