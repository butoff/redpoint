package com.romanbutov.redpoint.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.romanbutov.redpoint.provider.RedpointContract.Routes.*;

/**
 * Helper for SQLite database that stores data for {@link RedpointProvider}
 */
class RedpointSQLiteHelper extends SQLiteOpenHelper {
    private static final String TAG = "RedpointSQLiteHelper";

    // If you change the database schema, you must increment the database version.
    private static final int VERSION_ONE = 1;
    private static final int DATABASE_VERSION = VERSION_ONE;
    private static final String DATABASE_NAME = "redpoint.db";

    private CreatingListener creatingListener;

    public RedpointSQLiteHelper(Context context, CreatingListener creatingListener) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.creatingListener = creatingListener;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "onCreate: start");

        db.execSQL("CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + ROUTE_PK + " TEXT NOT NULL,"
                + ROUTE_NAME + " TEXT NOT NULL,"
                + ROUTE_GRADE + " INTEGER NOT NULL,"
                + ROUTE_SECTOR + " INTEGER NOT NULL,"
                + ROUTE_LOCATION_X + " INTEGER NOT NULL,"
                + ROUTE_LOCATION_Y + " INTEGER NOT NULL,"
                + ROUTE_AVATAR_URL + " TEXT,"
                + ROUTE_CREATE_DATE + " INTEGER,"
                + ROUTE_MODIFY_DATE + " INTEGER,"
                + ROUTE_STATUS + " INTEGER NOT NULL DEFAULT 0,"
                + ROUTE_NOTE + " TEXT,"
                + "UNIQUE (" + ROUTE_PK + ") ON CONFLICT REPLACE)"
        );
        // since local database is empty yet, we are trying to obtain all data from server
        creatingListener.onDatabaseNewlyCreated();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "onUpgrade() called with: db = [" + db + "], oldVersion = [" + oldVersion + "], newVersion = [" + newVersion + "]");
        // Since there is only first version of DB, do nothing
    }

    public interface CreatingListener {
        void onDatabaseNewlyCreated();
    }
}