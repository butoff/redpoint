package com.romanbutov.redpoint;

import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.romanbutov.redpoint.network.NetworkHelper;
import com.romanbutov.redpoint.network.RouteTransport;
import com.romanbutov.redpoint.network.RoutesLoadedCallback;

public class NewRouteActivity extends AppCompatActivity implements RoutesLoadedCallback {

    private ProgressBar progressBar;
    private TextView textView;
    private NetworkHelper network;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_route);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postRoute();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //network = new NetworkHelper(this);
        progressBar = (ProgressBar) findViewById(R.id.progress_new_route);
        progressBar.setVisibility(View.GONE);
        textView = (TextView) findViewById(R.id.message_new_route);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void postRoute() {
        progressBar.setVisibility(View.VISIBLE);
        RouteTransport route = new RouteTransport();
        route.avatar = "avatar";
        route.grade = 72;
        route.location_x = 30;
        route.location_y = 50;
        route.name = "Test New Route";
        network.addRoute(route);
    }

    private void showSnackbar(View view) {
        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void onRoutesLoaded() {
        progressBar.setVisibility(View.GONE);
        String successStr = network.isSuccess ? "success" : "failed";
        String msg = String.format("POST: %s, status=%d", successStr, network.status);
        if (network.message != null) {
            msg += ", message: " + network.message;
        }
        textView.setText(msg);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("NewRoute Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
