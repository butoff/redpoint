package com.romanbutov.redpoint;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.romanbutov.redpoint.util.Route;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * A fragment representing a single Route detail screen.
 * This fragment is either contained in a {@link RouteListActivity}
 * in two-pane mode (on tablets) or a {@link RouteDetailActivity}
 * on handsets.
 */
public class RouteDetailFragment extends Fragment {
    private static final String TAG = "RouteDetailFragment";

    private Route route;
    private LocationView locationView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public RouteDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            route = arguments.getParcelable(Route.class.getCanonicalName());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.route_detail, container, false);
        if (route != null) {
            ((TextView) rootView.findViewById(R.id.route_grade)).setText(route.getGradeStr());
            ((TextView) rootView.findViewById(R.id.route_date)).setText(route.getCreationDateStr());
            ((TextView) rootView.findViewById(R.id.route_location)).setText(route.getLocationStr());
        }
        locationView = (LocationView) rootView.findViewById(R.id.location_view);
        locationView.setRoute(route);
        return rootView;
    }
}
