package com.romanbutov.redpoint.util;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

/**
 * @author Roman Butov
 */
public class GradeTest {

    private Grade g6c = new Grade(6, Grade.Letter.C);
    private Grade g7aMinus = new Grade(7, Grade.Letter.A, Grade.Sign.MINUS);
    private Grade g7a = new Grade(7, Grade.Letter.A);
    private Grade g7aPlus = new Grade(7, Grade.Letter.A, Grade.Sign.PLUS);
    private Grade g7b = new Grade(7, Grade.Letter.B);

    @Test
    public void getNumeric() throws Exception {
        assertThat(g7a.getNumeric(), equalTo(72));
        assertThat(g7aPlus.getNumeric(), equalTo(73));
        assertThat(g7aMinus.getNumeric(), equalTo(71));
    }

    @Test
    public void next() throws Exception {
        assertThat(g6c.next(), equalTo(g7a));
        assertThat(g7a.next(), equalTo(g7b));
        assertThat(g7aMinus.next(), equalTo(g7b));
        assertThat(g7aPlus.next(), equalTo(g7b));
    }

    @Test
    public void getUnsigned() throws Exception {
        assertThat(g7aPlus.getUnsigned(), equalTo(g7a));
        assertThat(g7aMinus.getUnsigned(), equalTo(g7a));
    }

    @Test
    public void getPlus() throws Exception {
        Grade gplus = g7a.getPlus();
        assertThat(gplus, equalTo(g7aPlus));
        gplus = g7aPlus.getPlus();
        assertThat(gplus, equalTo(g7aPlus));
        gplus = g7aMinus.getPlus();
        assertThat(gplus, equalTo(g7aPlus));
    }

    @Test
    public void getMinus() throws Exception {
        Grade gminus = g7a.getMinus();
        assertThat(gminus, equalTo(g7aMinus));
    }

    @Test
    public void compareTo() throws Exception {
        assertThat(g7a.compareTo(g7aPlus) < 0, is(true));
        assertThat(g6c.compareTo(g7a) < 0, is(true));
        assertThat(g7aPlus.compareTo(g7aMinus) > 0, is(true));
    }

    @Test
    public void equals() throws Exception {
        Grade g = new Grade(7, Grade.Letter.A, Grade.Sign.PLUS);
        Grade g1 = new Grade(7, Grade.Letter.A);
        assertThat(g, equalTo(g7aPlus));
        assertThat(g1, not(g7aPlus));
    }

}