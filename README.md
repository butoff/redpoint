# README #


RedPoint mobile is a simple climbing guide application for android devices.

### Features: ###

* Providing list of routes in the climbing gym
* Providing detail information about each route, including grade, location in the gym, creation date
* Filtering routes by range of grades
* Periodically update, interaction with server with simple REST API

### Upcoming features: ###

* Providing short movies about routes
* Storing information about successfully climbed routes, including onsite and redpoint
* User authentication
* User comments and polls about routes